# tarea3

"NOTAAAA: LA ACTUALIZACION O CAMBIOS DEL CODIGO DE REALIZO EN EL ARCHIVO README.MD"
creacion de codigo en processing

import ddf.minim.*;
Minim minim;
AudioPlayer player;

int temper=0; //Variable temperatura inicializada en 0
int presion=0; //variable presion inicializada en 0
int tiempo1=6000; //variable tiempo

void setup(){
size(1200,960);//tamaño de la ventana

minim = new Minim(this);
player = minim.loadFile("alarma_5.mp3");

}
void draw(){

  int currentTime=millis();
 
background(0,50,50);  //FONDO GRISS
fill(255);   //color blanco para texto
textSize(50);            //tamaño de fuente para el sensor de presion
text("Presión", 220,130);
textSize(50);         //tamaño fuente para el sensor de temperatura
text("Temperatura", 670,130);


fill(255);
text(presion,280,170);
fill(255);
text(temper,780,170);

float distancia2 = dist(mouseX,mouseY,800,300);//variable entera para captar la posicion del mouse en los sensores
fill(0); stroke(100,0,0); strokeWeight(0 );//RELLENO Y COLOR DEL BORDE DE LA FIGURA
if ( distancia2 < 125){
temper=temper+1;//aumento de temperatura gradual
fill(200,0,200);// color del sensor cuando el cursos esta por encima
if (currentTime > tiempo1){player.play();}
}
else {temper = temper-1;} //condicion  si el mouse se encuentra fuera del area de la figura del sensor
if (temper<0){temper=0;}   //intervalo de 
if (temper>255){temper=255;}  temperatura//


ellipse(800, 300, 250, 250); //SESOR DE TEMPERATURA

float distancia = dist(mouseX,mouseY,300,300);//variable entera para captar la posicion del mouse en los sensores
fill(0); stroke(100,100,0); strokeWeight(0) ;//RELLENO Y COLOR DEL BORDE DE LA FIGURA
if ( distancia < 125){
  presion=presion+1;//aumento de temperatura gradual
fill(200,0,0);          // color del sensor cuando el cursos esta por encima
if (currentTime > tiempo1){player.play();}
}
else{presion=presion-1;} //condicion  si el mouse se encuentra fuera del area de la figura del sensor
if (presion<0){presion=0;}        // intervalo
if (presion>255){presion=255;}     de presion//

ellipse(300, 300,250, 250); //SENSOR DE PRESION


fill(0,presion,0);            //visualizador gradual del sensor de presion
ellipse(300,600, 150, 150); 

fill(temper,1,1);          //visualizador gradual sensor de temperatura
ellipse(800,600, 150, 150); 

if (presion==255){
  textSize(100);
  fill(255);
text("ALERTAAAAA!!!!!", 300,300);
text("ALERTAAAAA!!!!!", 300,600);
text("ALERTAAAAA!!!!!", 300,900);}

if (temper==255){
  fill(255);
  textSize(100);
  fill(255);
  text("ALERTAAAAA!!!!!", 300,300);
text("ALERTAAAAA!!!!!", 300,600);
text("ALERTAAAAA!!!!!", 300,900);
}



}
